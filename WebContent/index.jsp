<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    	               "http://www.w3.org/TR/html4/loose.dtd">

<html>
  <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>GlassFish JSP Page</title>
    	<script type="text/javascript" src="http://static.mstn.com/clearg/js/json2-min.js">
    	</script>
    	<script type="text/javascript">
    	
    	var a = {
		    "footer": [
		        [{
		                "text": "NOTAS",
		                "type": "label"
		            }
		        ]
		    ],
		    "body": [{
		            "columns": 1,
		            "type": "table",
		            "footer": [],
		            "detail": [
		                [{
		                        "text": "FACTURA",
		                        "type": "label"
		                    }
		                ],
		                [{
		                        "text": "",
		                        "type": "label"
		                    }
		                ],
		                [{
		                        "text": "GRID",
		                        "type": "label"
		                    }
		                ],
		                [{
		                        "text": "",
		                        "type": "label"
		                    }
		                ],

		                [
							{
							    "columns": 5,
							    "type": "table",
							    "footer": [],
							    "detail": [],
							    "header": [
									        [
									         {
									        	 "text": "Codigo",
									        	 "type": "label"
									         },
									         {
									        	 "text": "Descripcion",
									        	 "type": "label"
									         },
									         {
									        	 "text": "Cantidad",
									        	 "type": "label"
									         },
									         {
									        	 "text": "Precio",
									        	 "type": "label"
									         },
									         {
									        	 "text": "Sub-Total",
									        	 "type": "label"
									         }
									        ]
							    ],
							    "dataSet": "CGERP_AR_INVOICES"
							}
		                ]
		            ],
		            "header": [
		                [{
		                        "text": "Header Row",
		                        "type": "label"
		                    }
		                ]
		            ],
		            "dataSet": "CGERP_AR_INVOICES"
		        }
		    ],
		    "datasources": [],
		    "page": [],
		    "styles": [{
		            "fontWeight": "bold",
		            "color": "#FFFFFF",
		            "backgroundColor": "#008000",
		            "name": "LabelHeader",
		            "fontFamily": "Arial Black",
		            "fontSize": "13px"
		        }, {
		            "borderBottomStyle": "solid",
		            "backgroundColor": "#F7F7F7",
		            "name": "LabelRow1",
		            "borderBottomWidth": "1px",
		            "fontFamily": "Century",
		            "fontSize": "12px"
		        }, {
		            "borderBottomStyle": "solid",
		            "backgroundColor": "#E6E5E5",
		            "name": "LabelRow2",
		            "borderBottomWidth": "1px",
		            "fontFamily": "Century",
		            "fontSize": "12px"
		        }, {
		            "fontWeight": "bold",
		            "name": "Title",
		            "textAlign": "center",
		            "fontFamily": "Sans Serif",
		            "fontSize": "18px"
		        }
		    ],
		    "table": "CGERP_AR_INVOICES",
		    "header": []
		};
		alert(JSON.stringify(a));
    	</script>
  </head>
  <body>
    <h1>Hello World!</h1>
  </body>
</html> 
