package mstn.clearg.reports;
import java.util.logging.Level;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;


public class ReportApi {

	public void createReport(){
		EngineConfig config = new EngineConfig( );
		try{
					
			Platform.startup( config );  //If using RE API in Eclipse/RCP application this is not needed.
			IReportEngineFactory factory = (IReportEngineFactory) Platform
					.createFactoryObject( IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY );
			IReportEngine engine = factory.createReportEngine( config );
			engine.changeLogLevel( Level.WARNING );
			
		}catch( Exception ex){
			ex.printStackTrace();
		}
	}
}
