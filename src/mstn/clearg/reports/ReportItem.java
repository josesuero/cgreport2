package mstn.clearg.reports;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.DataItemHandle;
import org.eclipse.birt.report.model.api.DataSetHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.Expression;
import org.eclipse.birt.report.model.api.ExpressionType;
import org.eclipse.birt.report.model.api.GridHandle;
import org.eclipse.birt.report.model.api.ImageHandle;
import org.eclipse.birt.report.model.api.LabelHandle;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.ReportItemHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.SlotHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.TableHandle;
import org.eclipse.birt.report.model.api.TextDataHandle;
import org.eclipse.birt.report.model.api.TextItemHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.elements.structures.ComputedColumn;
import org.eclipse.birt.report.model.api.elements.structures.EmbeddedImage;
import org.eclipse.birt.report.model.api.elements.structures.ParamBinding;
import org.eclipse.birt.report.model.api.elements.structures.ResultSetColumn;
import org.eclipse.birt.report.model.elements.interfaces.IStyleModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class ReportItem {

	private ReportDesignHandle designHandle = null;
	private JSONObject element;
	private JSONArray meta;
	// private JSONArray data;
	private ReportItemHandle reportItem;
	private ElementFactory elementFactory;
	private String elementCount;
	private JSONArray reportTables;

	public ReportItem(ReportDesignHandle designHandle, JSONObject element,
			JSONArray meta, JSONArray reportTables) throws SemanticException,
			JSONException, IOException {
		this.designHandle = designHandle;
		this.element = element;
		this.meta = meta;
		this.elementFactory = designHandle.getElementFactory();
		this.reportTables = (JSONArray) reportTables.clone();
		createElement();
	}

	public ReportItem(ReportDesignHandle designHandle, JSONObject element,
			JSONArray meta) throws SemanticException, JSONException,
			IOException {
		this(designHandle, element, meta, new JSONArray());
	}

	public ReportItem(ReportDesignHandle designHandle, JSONObject element)
			throws SemanticException, JSONException, IOException {
		this(designHandle, element, new JSONArray(), new JSONArray());
	}

	public ReportItemHandle getReportItem() {
		return reportItem;
	}

	private void setReportItem(ReportItemHandle reportItem) {
		this.reportItem = reportItem;
	}

	private String concat(String... strs) {
		StringBuilder result = new StringBuilder();
		for (String string : strs) {
			result.append(string);
		}
		return result.toString();
	}

	private void createElement() throws SemanticException, JSONException,
			IOException {
		elementCount = designHandle.getComments();
		if (elementCount == null) {
			elementCount = "0";
		} else {
			elementCount = String.valueOf(Integer.valueOf(elementCount) + 1);
		}
		designHandle.setComments(elementCount);
		String type = element.getString("type");
		if (type.equalsIgnoreCase("table")) {
			if (element.has("auto") && element.getBoolean("auto") == true) {
				this.setReportItem(createAutoTable());
			} else {
				this.setReportItem(createTable());
			}
		} else if (type.equalsIgnoreCase("label")) {
			this.setReportItem(createLabel());
		} else if (type.equalsIgnoreCase("data")) {
			this.setReportItem(createData());
		} else if (type.equalsIgnoreCase("dynamictext")) {
			this.setReportItem(createDynamicText());
		} else if (type.equalsIgnoreCase("grid")) {
			this.setReportItem(createGrid());
		} else if (type.equalsIgnoreCase("text")) {
			this.setReportItem(createText());
		} else if (type.equalsIgnoreCase("image")) {
			this.setReportItem(createImage());
		}
		if (element.has("style")) {
			reportItem.setStyleName(element.getString("style"));
		}

		if (element.has("properties")) {
			JSONObject properties = element.getJSONObject("properties");
			for (int i = 0; i < properties.names().length(); i++) {
				String property = properties.names().getString(i);
				reportItem.setProperty(property, properties.get(property));
			}
		}

	}

	private ReportItemHandle createTable() throws JSONException,
			SemanticException, IOException {
		JSONArray headers = null;
		JSONArray details = null;
		JSONArray footers = null;
		int colCount = 1;

		if (element.has("header")) {
			headers = element.getJSONArray("header");
		} else {
			headers = new JSONArray();
		}

		if (element.has("detail")) {
			details = element.getJSONArray("detail");
		} else {
			details = new JSONArray();
		}

		if (element.has("footer")) {
			footers = element.getJSONArray("footer");
		} else {
			footers = new JSONArray();
		}

		if (element.has("columns")) {
			colCount = element.getInt("columns");
		}

		TableHandle table = elementFactory.newTableItem(concat("dataTable", elementCount), colCount, headers.length(), details.length(), footers.length());

		// Assign DataSet
		table.setProperty(TableHandle.DATA_SET_PROP, element.getString("dataSet"));

		// DataSet parameters
		if (element.has("detailData")) {
			ParamBinding paramBind = new ParamBinding();
			paramBind.setParamName("DETAILS");
			Expression ex = new Expression(concat("row[\"", element.getString("detailData"), "\"]"), ExpressionType.JAVASCRIPT);
			ArrayList<Expression> values = new ArrayList<Expression>();
			values.add(ex);
			paramBind.setExpression(values);

			PropertyHandle params = table.getPropertyHandle(TableHandle.PARAM_BINDINGS_PROP);
			params.addItem(paramBind);

		}

		DataSetHandle dataSetHandle = null;

		reportTables.add((new JSONObject()).put("name", element.getString("dataSet")));
		SlotHandle datasets = designHandle.getDataSets();

		for (int i = 0; i < datasets.getCount(); i++) {
			// ((DataSetHandle)
			// datasets.get(i)).getName().equals(element.getString("dataSet"))
			String dataSetName = ((DataSetHandle) datasets.get(i)).getName();
			int dataSetPos = reportTables.find("name", dataSetName);

			if (dataSetPos != -1) {
				dataSetHandle = (DataSetHandle) datasets.get(i);
				List resultSetCols = dataSetHandle.getListProperty(DataSetHandle.RESULT_SET_PROP);
				if (resultSetCols != null) {
					PropertyHandle boundCols = table.getColumnBindings();
					Iterator iterator = resultSetCols.iterator();
					while (iterator.hasNext()) {
						ResultSetColumn rsHandle = (ResultSetColumn) iterator.next();

						/*
						 * if (meta.getJSONObject(meta.find("name",
						 * rsHandle.getColumnName()))
						 * .getString("type").equalsIgnoreCase("CGTABLE")) {
						 * continue; }
						 */

						if (rsHandle.getDataType().equalsIgnoreCase("blob")) {
							continue;
						}

						StringBuilder expression = new StringBuilder();
						StringBuilder tablePrefix = new StringBuilder("");
						if (dataSetPos == (reportTables.length() - 1)) {
							expression.append("dataSetRow");
						} else {
							tablePrefix.append(dataSetName).append("_");
							expression.append("row");
							int repeats = reportTables.length() - dataSetPos - 1;
							for (int x = 0; x < repeats; x++) {
								expression.append("._outer");
							}
						}
						expression.append("[\"").append(rsHandle.getColumnName()).append("\"]");

						ComputedColumn col = StructureFactory.createComputedColumn();
						col.setName(concat(tablePrefix.toString(),rsHandle.getColumnName()));

						col.setExpression(expression.toString());
						boundCols.addItem(col);
					}
				}
			}
		}

		for (int i = 0; i < headers.length(); i++) {
			JSONArray headerRow = headers.getJSONArray(i);
			RowHandle row = (RowHandle) table.getHeader().get(i);
			for (int j = 0; j < headerRow.length(); j++) {
				CellHandle cell = (CellHandle) row.getCells().get(j);
				cell.addElement(new ReportItem(designHandle,headerRow.getJSONObject(j), new JSONArray(),reportTables).getReportItem(), 0);
			}

		}

		for (int i = 0; i < details.length(); i++) {
			JSONArray detailRow = details.getJSONArray(i);
			RowHandle row = (RowHandle) table.getDetail().get(i);
			for (int j = 0; j < detailRow.length(); j++) {
				CellHandle cell = (CellHandle) row.getCells().get(j);
				cell.addElement(new ReportItem(designHandle, detailRow.getJSONObject(j), new JSONArray(), reportTables).getReportItem(), 0);
			}
		}

		for (int i = 0; i < footers.length(); i++) {
			JSONArray footerRow = footers.getJSONArray(i);
			RowHandle row = (RowHandle) table.getFooter().get(i);
			for (int j = 0; j < footerRow.length(); j++) {
				CellHandle cell = (CellHandle) row.getCells().get(j);
				cell.addElement(new ReportItem(designHandle,footerRow.getJSONObject(j), new JSONArray(),reportTables).getReportItem(), 0);
			}

		}

		return table;

	}

	private ReportItemHandle createAutoTable() throws SemanticException,
			JSONException {

		// create a table and set it up, add the table to the design
		TableHandle table = elementFactory.newTableItem("dataTable",
				meta.length(), 1, 1, 1);
		table.setProperty(IStyleModel.TEXT_ALIGN_PROP,
				DesignChoiceConstants.TEXT_ALIGN_CENTER);
		table.setWidth("100%");
		table.setProperty(TableHandle.DATA_SET_PROP,
				element.getString("dataSet"));
		// designHandle.getBody().add(table);
		// set the width of the first column to 3 inch
		// ColumnHandle ch = (ColumnHandle) table.getColumns().get(0);
		// ch.getWidth().setStringValue("3in");

		// bind the data set columns to the table

		DataSetHandle dataSetHandle = null;

		SlotHandle datasets = designHandle.getDataSets();
		for (int i = 0; i < datasets.getCount(); i++) {

			if (((DataSetHandle) datasets.get(i)).getName().equals(
					element.getString("dataSet"))) {
				dataSetHandle = (DataSetHandle) datasets.get(i);
				break;
			}
		}

		List resultSetCols = dataSetHandle
				.getListProperty(DataSetHandle.RESULT_SET_PROP);
		PropertyHandle boundCols = table.getColumnBindings();
		for (Iterator iterator = resultSetCols.iterator(); iterator.hasNext();) {
			ResultSetColumn rsHandle = (ResultSetColumn) iterator.next();

			if (meta.getJSONObject(meta.find("name", rsHandle.getColumnName()))
					.getString("type").equalsIgnoreCase("CGTABLE")) {
				continue;
			}

			ComputedColumn col = StructureFactory.createComputedColumn();
			col.setName(concat("column_", rsHandle.getColumnName()));

			StringBuilder expression = new StringBuilder("dataSetRow[\"");
			expression.append(rsHandle.getColumnName()).append("\"]");

			col.setExpression(expression.toString());
			boundCols.addItem(col);
		}

		// These element handles will be used to populate the various
		// table rows, cells, labels, and dataItems
		RowHandle rowHandle;
		CellHandle cellHandle;
		LabelHandle labelHandle;
		DataItemHandle dataHandle;

		{ // Create a Header Row Filled with labels
			rowHandle = (RowHandle) table.getHeader().get(0);

			for (int i = 0; i < meta.length(); i++) {
				JSONObject metaField = meta.getJSONObject(i);
				if (metaField.getString("type").equalsIgnoreCase("CGTABLE")) {
					continue;
				}
				cellHandle = (CellHandle) rowHandle.getCells().get(i);
				labelHandle = elementFactory.newLabel(concat("label",
						metaField.getString("name")));
				labelHandle.setText(metaField.getString("name"));
				cellHandle.getContent().add(labelHandle);
				cellHandle.setStyleName("LabelHeader");

				// ColumnHandle ch = (ColumnHandle) table.getColumns().get(i);
				// ch.getWidth().setStringValue((metaField.getString("name").length()
				// * 13) + "px");
			}

		}

		// BIND PARAMETER TO FIELD
		/*
		 * DO NOT DELETE ParamBinding paramBind = new ParamBinding();
		 * paramBind.setParamName("DETAILS");
		 * 
		 * Expression ex = new Expression("row[\"CGERP_AR_INVOICES_DETAILS\"]",
		 * ExpressionType.JAVASCRIPT); ArrayList<Expression> values = new
		 * ArrayList<Expression>(); values.add(ex);
		 * paramBind.setExpression(values);
		 * 
		 * PropertyHandle params =
		 * table.getPropertyHandle(TableHandle.PARAM_BINDINGS_PROP);
		 * params.addItem(paramBind);
		 */

		{ // Add DataItems to the Detail Row
			rowHandle = (RowHandle) table.getDetail().get(0);

			// int spyRow = 0;
			for (int i = 0; i < meta.length(); i++) {
				JSONObject metaField = meta.getJSONObject(i);
				if (metaField.getString("type").equalsIgnoreCase("CGTABLE")) {
					continue;
				}
				cellHandle = (CellHandle) rowHandle.getCells().get(i);
				dataHandle = elementFactory.newDataItem(metaField
						.getString("name"));
				dataHandle.setResultSetColumn(concat("column_",
						metaField.getString("name")));
				cellHandle.getContent().add(dataHandle);
				cellHandle.setStyleName("LabelRow1");
				/*
				 * if (spyRow == 0) { cellHandle.setStyleName("LabelRow1");
				 * spyRow++; } else { cellHandle.setStyleName("LabelRow2");
				 * spyRow = 0; }
				 */
			}

		}
		/**/
		return table;
	}

	private ReportItemHandle createLabel() throws SemanticException,
			JSONException {
		LabelHandle labelHandle = elementFactory.newLabel(concat("label",
				elementCount));
		labelHandle.setText(element.getString("text"));
		return labelHandle;
	}

	private ReportItemHandle createData() throws SemanticException,
			JSONException {
		String columnName = element.getString("column");
		DataItemHandle data = elementFactory.newDataItem(concat("data_",
				columnName, elementCount));
		data.setResultSetColumn(columnName);
		return data;
	}

	private ReportItemHandle createDynamicText() throws SemanticException,
			JSONException {
		String expr = element.getString("text");
		String contentType = "HTML";
		if (element.has("contenttype")) {
			contentType = element.getString("contenttype");
		}

		TextDataHandle text = elementFactory.newTextData(concat("DynamicText",
				elementCount));
		text.setValueExpr(expr);
		text.setContentType(contentType);

		return text;
	}

	private ReportItemHandle createText() throws SemanticException,
			JSONException {

		String expr = element.getString("text");
		String contentType = "HTML";
		if (element.has("contenttype")) {
			contentType = element.getString("contenttype");
		}

		TextItemHandle text = elementFactory.newTextItem(concat("Text",
				elementCount));
		text.setContentType(contentType);
		text.setContent(expr);

		return text;
	}

	private ReportItemHandle createGrid() throws JSONException,
			SemanticException, IOException {
		int columnNum = 1, rowNum = 1;
		if (element.has("columns")) {
			columnNum = element.getInt("columns");
		}
		if (element.has("rows")) {
			rowNum = element.getInt("rows");
		}
		GridHandle grid = elementFactory.newGridItem(
				concat("grid", elementCount), columnNum, rowNum);

		if (element.has("detail")) {
			JSONArray details = element.getJSONArray("detail");
			for (int i = 0; i < details.length(); i++) {
				JSONArray detailRow = details.getJSONArray(i);
				RowHandle row = (RowHandle) grid.getRows().get(i);
				for (int j = 0; j < detailRow.length(); j++) {
					CellHandle cell = (CellHandle) row.getCells().get(j);
					cell.addElement(
							new ReportItem(designHandle, detailRow
									.getJSONObject(j), new JSONArray(),
									reportTables).getReportItem(), 0);
				}
			}
		}

		return grid;
	}

	private ReportItemHandle createImage() throws IOException,
			SemanticException, JSONException {

		ImageHandle imageHandle = elementFactory.newImage(concat("image",
				elementCount));

		URL myurl = new URL(element.getString("url"));
		BufferedImage img = ImageIO.read(myurl);
		ByteArrayOutputStream bas = new ByteArrayOutputStream();
		ImageIO.write(img, "png", bas);

		EmbeddedImage image = StructureFactory.createEmbeddedImage();
		image.setType(DesignChoiceConstants.IMAGE_TYPE_IMAGE_PNG);
		image.setData(bas.toByteArray());
		image.setName(concat("Attachment", elementCount));
		designHandle.addImage(image);

		imageHandle.setSource(DesignChoiceConstants.IMAGE_REF_TYPE_EMBED);
		imageHandle.setImageName(concat("Attachment", elementCount));

		return imageHandle;
	}
}
