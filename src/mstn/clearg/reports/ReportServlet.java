package mstn.clearg.reports;
import java.io.IOException;
import java.net.URL;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;

import java.util.logging.Level;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.engine.api.EngineConfig;
import org.eclipse.birt.report.engine.api.EngineConstants;
import org.eclipse.birt.report.engine.api.HTMLRenderOption;
import org.eclipse.birt.report.engine.api.IReportEngine;
import org.eclipse.birt.report.engine.api.IReportEngineFactory;
import org.eclipse.birt.report.engine.api.IReportRunnable;
import org.eclipse.birt.report.engine.api.IRunAndRenderTask;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.core.internal.registry.RegistryProviderFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Servlet implementation class DeDemoServlet
 */
@WebServlet("/report")
@SuppressWarnings("unchecked")
public class ReportServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private 

	IReportEngineFactory factory;
	IReportEngine engine;
	private static InitialContext initialContext;
	private ServletOutputStream outputStream;
	
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ReportServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		EngineConfig config = new EngineConfig();
		try {

			Platform.startup(config);
			factory = (IReportEngineFactory) Platform
					.createFactoryObject(IReportEngineFactory.EXTENSION_REPORT_ENGINE_FACTORY);
			engine = factory.createReportEngine(config);
			engine.changeLogLevel(Level.WARNING);
			initialContext = new InitialContext();
		} catch (Exception ex) {
			try {
				outputStream.print(ex.getMessage() + ex.getCause());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setHeader("Access-Control-Allow-Origin", "*");
			outputStream = response.getOutputStream();
			genReport(request, response);
		} catch (Exception ex) {
			try {
				StringBuilder error = new StringBuilder();
				error.append(ex.getMessage());
				error.append(ex.getCause());
				for (int i =0;i < ex.getStackTrace().length;i++){
					error.append(ex.getStackTrace()[i]);
				}
				outputStream.print(error.toString());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void genReport(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String format = request.getParameter("format");
		String templateStr = request.getParameter("template");
		
		if (format == null || format.isEmpty() || format.equalsIgnoreCase("html")) {
			// HTML
			format = "html";
			response.setContentType("text/html");
		} else if (format.equalsIgnoreCase("pdf")) {
			// PDF
			format = "pdf";
			response.setContentType("application/pdf");
		} else if (format.equalsIgnoreCase("xls")) {
			StringBuilder contentDisposition = new StringBuilder(
					"attachment; filename=");
			String file = request.getParameter("filename");
			if (file == null) {
				file = "report.xls";
			}
			contentDisposition.append(file);
			// Excel
			response.setContentType("application/vnd.ms-excel");
			response.setHeader("Content-Disposition",
					contentDisposition.toString());
		} else {
			throw new Exception("Format not supported");
		}

		JSONObject template = new JSONObject(templateStr);
		//JSONObject template = getTemplate(request.getParameter("template"));
		
		//JSONObject data = getData();
		JSONObject data = new JSONObject(request.getParameter("data"));
					
		ReportDesignHandle report = (new TableReport(template,data).createReport());
		
		if(request.getParameter("save") != null && request.getParameter("save").equals("true")){
			report.saveAs(getServletContext().getRealPath("/") + "temp.rptdesign");
		}
		//ReportDesignHandle report = DesignApi.buildReport();
		IReportRunnable design = engine.openReportDesign(report);

		// Create task to run and render the report,
		IRunAndRenderTask task = engine.createRunAndRenderTask(design);
		// Set parent classloader for engine
		task.getAppContext().put(
				EngineConstants.APPCONTEXT_CLASSLOADER_KEY,
				this.getClass().getClassLoader());
		
	
		// Set parameter values and validate
		task.setParameterValue("data", data.getJSONArray("data").toString());
		//task.setParameterValue("Top Count", (new Integer(5)));
		//task.validateParameters();

		// Setup rendering to HTML
		HTMLRenderOption options = new HTMLRenderOption();
		// options.setOutputFileName("output/resample/TopNPercent.html");
		// options.setOutputFormat("html");
		options.setOutputFormat(format);
		options.setOutputStream(outputStream);
		// Setting this to true removes html and body tags
		options.setEmbeddable(true);

		task.setRenderOption(options);
		// run and render report
		task.run();
		task.close();
		
		report.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	@Override
	public void destroy() {
		super.destroy();
		engine.destroy();
		Platform.shutdown();
		// Bugzilla 351052
		RegistryProviderFactory.releaseDefault();
	}

	public void render() {

	}
	
	public JSONObject getTemplate(String templateName){
		JSONObject template;
		 String response = "";
		   try { 
		        URL url = new URL(getGlobalVar("cgApp-Url") + "server?template=CGERP.AR_INVOICES&data=[]");
		        response = IOUtils.toString(url.openStream(), "UTF-8");
		        template = new JSONObject(response);
		        template.get("content");
		    } catch(Exception e) { 
		        throw new RuntimeException(e); 
		    } 
		
		
		
		return template;
	}
	
	public JSONObject getTemplate() throws JSONException{
		JSONObject template = new JSONObject();

		template.put("header", new JSONArray());
		template.put("footer", new JSONArray());
		template.put("body", new JSONArray());
		template.put("styles", new JSONArray());
		template.put("datasources", new JSONArray());
		template.put("page", new JSONArray());
		template.put("table","CGERP_AR_INVOICES");
		
		//Body
		JSONObject element = new JSONObject();
		element.put("type", "image");
		element.put("url", "https://www.google.com/images/srpr/logo2w.png");
		element.put("style", "Title");		
		template.getJSONArray("body").put(element);
		
		JSONArray tableDetails = new JSONArray();
		tableDetails.add(new JSONArray());
		
		element = new JSONObject();
		element.put("type", "label");
		element.put("text", "ID");
		tableDetails.getJSONArray(0).put(element);
		
		element = new JSONObject();
		element.put("type", "label");
		element.put("text", "INVDATE");
		tableDetails.getJSONArray(0).put(element);
		
		element = new JSONObject();
		element.put("type", "grid");
		element.put("dataSet", "CGERP_AR_INVOICES");
		element.put("columns", 2);
		element.put("header", tableDetails);
		element.put("detail", tableDetails);
		element.put("footer", tableDetails);
		template.getJSONArray("body").put(element);
		
		//Styles
		JSONObject style = new JSONObject();
		style.put("name", "LabelHeader");
		style.put("fontWeight","bold");
		style.put("fontFamily","Arial Black");
		style.put("fontSize","13px");
		style.put("backgroundColor","#008000");
		style.put("color","#FFFFFF");
		
		template.getJSONArray("styles").put(style);
		
		style = new JSONObject();
		style.put("name", "LabelRow1");		
		style.put("fontFamily","Century");
		style.put("fontSize","12px");
		style.put("backgroundColor","#F7F7F7");
		style.put("borderBottomStyle","solid");
		style.put("borderBottomWidth","1px");
		
		template.getJSONArray("styles").put(style);
		
		style = new JSONObject();
		style.put("name", "LabelRow2");		
		style.put("fontFamily","Century");
		style.put("fontSize","12px");
		style.put("backgroundColor","#E6E5E5");
		style.put("borderBottomStyle","solid");
		style.put("borderBottomWidth","1px");
		template.getJSONArray("styles").put(style);
		
		style = new JSONObject();
		style.put("name", "Title");		
		style.put("fontFamily","Sans Serif");
		style.put("fontSize","18px");
		style.put("textAlign","center");		
		style.put("fontWeight","bold");
		template.getJSONArray("styles").put(style);
		
		return	new JSONObject("{'columns':1,'type':'table','footer':[],'body':[{'columns':1,'type':'table','footer':[[{'text':'','type':'label'}],[{'column':'NOTAS','type':'data'}]],'detail':[[{'text':'FACTURA','type':'label'}],[{'text':'','type':'label'}],[{'text':'GRID','type':'label'}],[{'text':'','type':'label'}],[{'columns':5,'type':'table','detailData':'CGERP_AR_INVOICES_DETAILS','dataSet':'CGERP_AR_INVOICES_DETAILS','header':[],'footer':[],'detail':[[{'text':'Codigo','type':'label','style':'LabelHeader'},{'text':'Descripcion','type':'label','style':'LabelHeader'},{'text':'Cantidad','type':'label','style':'LabelHeader'},{'text':'Precio','type':'label','style':'LabelHeader'},{'text':'Sub-Total','type':'label','style':'LabelHeader'}],[{'column':'CODE','type':'data'},{'column':'DESCRIPTION','type':'data'},{'column':'AMOUNT','type':'data'},{'column':'PRICE','type':'data'},{'column':'SUBTOTAL','type':'data'}]]}],[{'text':'','type':'label'}],[{'columns':5,'rows':5,'type':'grid','dataSet':'','header':[],'footer':[],'detail':[[{'text':'','type':'label'},{'text':'','type':'label'},{'text':'','type':'label'},{'text':'Sub-Total','type':'label','style':'LabelHeader'},{'column':'SUBTOTAL','type':'data'}],[{'text':'','type':'label'},{'text':'','type':'label'},{'text':'','type':'label'},{'text':'Descuento','type':'label','style':'LabelHeader'},{'column':'DISCOUNT','type':'data'}],[{'text':'','type':'label'},{'text':'','type':'label'},{'text':'','type':'label'},{'text':'ITBIS','type':'label','style':'LabelHeader'},{'column':'TAX','type':'data'}],[{'text':'','type':'label'},{'text':'','type':'label'},{'text':'','type':'label'},{'text':'Total','type':'label','style':'LabelHeader'},{'column':'TOTAL','type':'data'}],[{'text':'','type':'label'},{'text':'','type':'label'},{'text':'','type':'label'},{'text':'Pendiente','type':'label','style':'LabelHeader'},{'column':'PAYREMAIN','type':'data'}]]}]],'header':[[{'text':'','type':'label'}]],'dataSet':'CGERP_AR_INVOICES'}],'datasources':[],'page':[],'styles':[{'fontWeight':'bold','backgroundColor':'Silver','name':'LabelHeader','textAlign':'center','fontFamily':'Sans Serif','fontSize':'15px'},{'fontWeight':'bold','color':'#FFFFFF','backgroundColor':'#008000','name':'LabelHeader2','fontFamily':'Arial Black','fontSize':'13px'},{'borderBottomStyle':'solid','backgroundColor':'#F7F7F7','name':'LabelRow1','borderBottomWidth':'1px','fontFamily':'Century','fontSize':'12px'},{'borderBottomStyle':'solid','backgroundColor':'#E6E5E5','name':'LabelRow2','borderBottomWidth':'1px','fontFamily':'Century','fontSize':'12px'},{'fontWeight':'bold','name':'Title','textAlign':'center','fontFamily':'Sans Serif','fontSize':'18px'}],'table':'CGERP_AR_INVOICES','header':[]}");
	}
	
	public JSONObject getData() throws JSONException{
		
		JSONObject data = new JSONObject("{\"data\":[{\"ID_WAREHOUSE\":\"1\",\"TAX\":\"3344.0000\",\"ID_PRICE_GROUPS\":\"1\",\"INVDATE\":\"2011-02-02\",\"PONUMBER\":\"\",\"DISCOUNTPERC\":\"0.0000\",\"ID_PARTNER\":\"1\",\"TOTAL\":\"24244.0000\",\"PAYAMOUNT\":\"0.0000\",\"ID_STATUS\":\"1\",\"ID_TAX_TYPE\":\"1\",\"ID_SALES_ORDER\":\"0\",\"SUBTOTAL\":\"20900.0000\",\"PAYREMAIN\":\"24244.0000\",\"ID_SALES_CHANNEL\":\"1\",\"CGERP_AR_INVOICES_DETAILS\":{\"data\":[{\"ID_ITEM\":\"2\",\"TAX\":\"3344.0000\",\"PRICE\":\"100.0000\",\"SUBTOTAL\":\"20900.0000\",\"DESCRIPTION\":\"SERVICIO DE HOSTING\",\"ID\":\"1\",\"COST\":\"0.0000\",\"TOTAL\":\"24244.0000\",\"AMOUNT\":\"209.0000\",\"DISCOUNT\":\"0.0000\",\"ID_INVOICE\":\"1\"}],\"meta\":[{\"name\":\"ID\",\"type\":\"INT\",\"size\":11},{\"name\":\"ID_INVOICE\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_ITEM\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"DESCRIPTION\",\"type\":\"VARCHAR\",\"size\":2000},{\"name\":\"AMOUNT\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"COST\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"PRICE\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"SUBTOTAL\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"TAX\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"DISCOUNT\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"TOTAL\",\"type\":\"DECIMAL\",\"size\":20}]},\"ID\":\"1\",\"INVOICENO\":\"1\",\"TAXNUMBER\":\"A010010010100000001\",\"TAXPERC\":\"0.0000\",\"DISCOUNT\":\"0.0000\",\"NOTES\":\"\"}],\"meta\":[{\"name\":\"ID\",\"type\":\"INT\",\"size\":11},{\"name\":\"INVOICENO\",\"type\":\"VARCHAR\",\"size\":10},{\"name\":\"ID_PARTNER\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"INVDATE\",\"type\":\"DATE\",\"size\":10},{\"name\":\"TAXNUMBER\",\"type\":\"VARCHAR\",\"size\":255},{\"name\":\"ID_TAX_TYPE\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_PRICE_GROUPS\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_SALES_CHANNEL\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_STATUS\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_SALES_ORDER\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"ID_WAREHOUSE\",\"type\":\"DECIMAL\",\"size\":11},{\"name\":\"SUBTOTAL\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"TAX\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"DISCOUNT\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"TAXPERC\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"DISCOUNTPERC\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"TOTAL\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"PAYAMOUNT\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"PAYREMAIN\",\"type\":\"DECIMAL\",\"size\":20},{\"name\":\"PONUMBER\",\"type\":\"VARCHAR\",\"size\":255},{\"name\":\"NOTES\",\"type\":\"VARCHAR\",\"size\":2147483647},{\"name\":\"CGERP_AR_INVOICES_DETAILS\",\"type\":\"CGTABLE\",\"size\":0}]}");
		//return data.getJSONArray("data").getJSONObject(0).getJSONObject("CGERP_AR_INVOICES_DETAILS");
		return data;
	}

	public static String getGlobalVar(String name) {
		try {
		return (String)initialContext.lookup(name);
		} catch(Exception e){
			return null;
		}
	}
}
