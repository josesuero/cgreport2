package mstn.clearg.reports;

import java.io.IOException;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.model.api.DesignConfig;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.IDesignEngine;
import org.eclipse.birt.report.model.api.IDesignEngineFactory;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.ScalarParameterHandle;
import org.eclipse.birt.report.model.api.ScriptDataSetHandle;
import org.eclipse.birt.report.model.api.ScriptDataSourceHandle;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.StyleHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.command.ContentException;
import org.eclipse.birt.report.model.api.command.NameException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.elements.structures.DataSetParameter;
import org.eclipse.birt.report.model.api.elements.structures.ResultSetColumn;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ibm.icu.util.ULocale;

@SuppressWarnings({"unchecked"})
public class TableReport {

	ReportDesignHandle designHandle = null;
	IDesignEngine designEngine = null;
	ElementFactory designFactory = null;
	JSONObject cgdata = null;
	JSONObject template;

	public TableReport(JSONObject template, JSONObject data) throws JSONException {
		this.cgdata = data;
		this.template = template;
	}

	public ReportDesignHandle createReport() throws Exception {
		
		
		JSONArray data = this.cgdata.getJSONArray("data");
		JSONArray meta = this.cgdata.getJSONArray("meta");
		
		// Create a session handle. This is used to manage all open designs.
		// Your app need create the session only once.

		// Configure the Engine and start the Platform
		DesignConfig config = new DesignConfig();

		// config.setProperty("BIRT_HOME",
		// "/Users/josesuero/Software/birt-runtime-4_2_2/ReportEngine");
		IDesignEngine engine = null;

		Platform.startup();
		IDesignEngineFactory factory = (IDesignEngineFactory) Platform
				.createFactoryObject(IDesignEngineFactory.EXTENSION_DESIGN_ENGINE_FACTORY);
		engine = factory.createDesignEngine(config);

		SessionHandle session = engine.newSessionHandle(ULocale.ENGLISH);

		designHandle = session.createDesign();

		designFactory = designHandle.getElementFactory();

		DesignElementHandle simpleMasterPage = designFactory
				.newSimpleMasterPage("Master Page");//$NON-NLS-1$
		
		
		designHandle.getMasterPages().add(simpleMasterPage);

		
		//designHandle.getMasterPages().get(0).setProperty("orientation", "landscape");
		
		
		ScalarParameterHandle scalarParameterHandle = designHandle.getElementFactory().newScalarParameter("data"); 
        scalarParameterHandle.setValueType(DesignChoiceConstants.PARAM_VALUE_TYPE_STATIC); 
        scalarParameterHandle.setDataType(DesignChoiceConstants.PARAM_TYPE_STRING); 
        scalarParameterHandle.setParamType("simple"); 
        scalarParameterHandle.setPromptText("data"); 
        scalarParameterHandle.setControlType(DesignChoiceConstants.PARAM_CONTROL_TEXT_BOX); 
        
        designHandle.getParameters().add(scalarParameterHandle); 

		
        designHandle.setProperty("layoutPreference", "auto layout");
        
		createDataSources();

		createStyles();

		createDataSets(this.template.getString("table"),meta, data,false);

		createBody(meta,data);

		// designHandle.saveAs(System.getProperty("java.io.tmpdir") +
		// File.separator + rep);

		// designHandle.close();
		// Platform.shutdown();

		// System.out.println("Finished");

		return designHandle;

	}
	
	private void createDataSources() throws SemanticException {

		ScriptDataSourceHandle dataSourceHandle = designHandle
				.getElementFactory().newScriptDataSource("srcScripted");
		designHandle.getDataSources().add(dataSourceHandle);
	}

	private void createDataSets(String name,JSONArray meta,JSONArray data,boolean child) throws SemanticException, JSONException {

		// Data Set
		ScriptDataSetHandle dataSetHandle = designHandle.getElementFactory()
				.newScriptDataSet(name);
		dataSetHandle.setDataSource("srcScripted");

		// Set open( ) in code
		StringBuffer openCode = null;
		if (!child){
			openCode = generateOpenCode(name,meta,data);
		} else {
			openCode = generateChildOpenCode(name,meta, data);
			//Details parameter
			/*
			ParameterDefinition inputParamDefn = new ParameterDefinition( "DETAILS",org.eclipse.birt.core.data.DataType. STRING_TYPE );
			inputParamDefn.setInputMode( true );
			inputParamDefn.setPosition( 1 );
			inputParamDefn.setDefaultInputValue("");
			PropertyHandle params = dataSetHandle.getPropertyHandle(ScriptDataSetHandle.PARAMETERS_PROP);
			params.add(inputParamDefn);
			*/
			
			DataSetParameter dataSetParameter=StructureFactory.createDataSetParameter();
			dataSetParameter.setName("DETAILS");
			dataSetParameter.setDataType(DesignChoiceConstants.PARAM_TYPE_STRING);
			dataSetParameter.setIsInput(true);
			dataSetParameter.setDefaultValue("'{}'");
			dataSetParameter.setPosition(1);
			dataSetParameter.setIsOutput(false);
			PropertyHandle params = dataSetHandle.getPropertyHandle(ScriptDataSetHandle.PARAMETERS_PROP);
			params.addItem(dataSetParameter);       
		
		}
		dataSetHandle.setOpen(openCode.toString());

		// Set fetch( ) in code
		StringBuffer fetchCode = generateFetchCode(name,meta,data);
		dataSetHandle.setFetch(fetchCode.toString());

		PropertyHandle computedSet = dataSetHandle
				.getPropertyHandle(ScriptDataSetHandle.RESULT_SET_PROP);

		// Since this is a Scripted Data Source you need to tell the
		// DataSet what the columns are. For this example, we will just
		// hard code the known resultColumn values

		ResultSetColumn resultColumn = StructureFactory.createResultSetColumn();
		JSONArray innerDS = new JSONArray();
		for (int i = 0; i < meta.length(); i++) {
			JSONObject field = meta.getJSONObject(i);
			
			resultColumn = StructureFactory.createResultSetColumn();
			resultColumn.setPosition(i);
			resultColumn.setColumnName(meta.getJSONObject(i).getString("name"));
			resultColumn.setDataType("string");
			
			if (field.getString("type").equalsIgnoreCase("INT")){
				resultColumn.setDataType("integer");
			} else if (field.getString("type").equalsIgnoreCase("DECIMAL")){
				resultColumn.setDataType("decimal");
			} else if (field.getString("type").equalsIgnoreCase("DATE")){
				resultColumn.setDataType("date");
			} else if (field.getString("type").equalsIgnoreCase("CGTABLE")){
				innerDS.add(field);
				resultColumn.setDataType("blob");
			}
			computedSet.addItem(resultColumn);
		}
		designHandle.getDataSets().add(dataSetHandle);
		for (int i = 0;i < innerDS.length();i++) {
			JSONObject field  = innerDS.getJSONObject(i);

			JSONArray innermeta = new JSONArray();
			JSONArray innerdata = new JSONArray();

			if (data.length() > 0){
				JSONObject innerobj = data.getJSONObject(0).getJSONObject(field.getString("name"));
				innermeta = innerobj.getJSONArray("meta");
				innerdata = innerobj.getJSONArray("data");
			}
			createDataSets(field.getString("name"),innermeta, innerdata,true);
		}
	}

	private StringBuffer generateFetchCode(String name,JSONArray meta,JSONArray data) throws JSONException {
		StringBuffer fetchCode = new StringBuffer();
		fetchCode.append("if ( count");
		fetchCode.append(name);
		fetchCode.append(" < sourcedata");
		fetchCode.append(name);
		fetchCode.append(".length ){").append("\r\t");

		//int size = this.getSize(meta);
		
		//for (int i = 0; i < data.length(); i++) {
			for (int j = 0; j < meta.length(); j++) {
				fetchCode.append("row[\"")
				.append(meta.getJSONObject(j).getString("name"))
				.append("\"] = sourcedata").append(name);
				fetchCode.append("[count")
				.append(name)
				.append("][")
				.append(j)
				.append("];")
				.append("\r\t");
			}

		//}

		fetchCode.append("count").append(name).append("++;").append("\r\t");
		fetchCode.append("return true;").append("\r");
		fetchCode.append("} else {").append("\r\t");
		fetchCode.append("return false;").append("\r");
		fetchCode.append("};");
		return fetchCode;
	}
	/*
	private int getSize(JSONArray meta, JSONArray data) {

		int numCol = meta.length();

		for (int i = 1; i < data.length(); i++) {
			if ((numCol * i) > 100) {
				return i;
			}
		}

		return data.length();
	}
	*/
	private StringBuffer generateOpenCode(String name,JSONArray meta,JSONArray data) throws JSONException {

		StringBuffer openCode = new StringBuffer();

		openCode.append("count").append(name).append(" = 0;").append("\r");

		StringBuilder tmp = new StringBuilder("sourcedata").append(name).append(" = [];");
		
		//int numCol = meta.length();

		/*
		for (int i = 0; i < data.length(); i++) {
			if ((i+1) == data.length()) {
				tmp.append("new Array(")
				.append(numCol)
				.append("));");
			} else {
				tmp.append("new Array(")
				.append(numCol)
				.append("),");
			}
		}
		*/

		openCode.append(tmp).append("\r\r");

		openCode.append("var data = eval(\"(\" + params[\"data\"].value + \")\")");
		//openCode.append(this.cgdata.getJSONArray("data").toString());
		
		openCode.append(";\r");
		
		openCode.append("for(var i =0;i < data.length;i++){\r");
		openCode.append("sourcedata").append(name).append("[i] = [];\r");
		//for (int j = 0; j < data.length(); j++) {	
			for (int i = 0; i < meta.length(); i++) {
				
				openCode.append("sourcedata").append(name).append("[")
				.append("i")
				.append("][")
				.append(i)
				.append("]=")
				.append("data[i]").append("['").append(meta.getJSONObject(i).getString("name")).append("']")
				//data.getJSONObject(j).getString(meta.getJSONObject(i).getString("name")).replace("\"", "\\\"")
				.append(";").append("\r");
			}
		//}
		openCode.append("}\r");

		return openCode;

	}

	private StringBuffer generateChildOpenCode(String name,JSONArray meta,JSONArray data) throws JSONException {

		StringBuffer openCode = new StringBuffer();

		openCode.append("count").append(name).append(" = 0;").append("\r");

		StringBuilder tmp = new StringBuilder("sourcedata").append(name).append(" = new Array(");
		
		int numCol = meta.length();

		if (data.length() == 0){
			tmp.append("new Array(")
			.append(numCol)
			.append("));");
		}
		
		for (int i = 0; i < data.length(); i++) {
			if ((i+1) == data.length()) {
				tmp.append("new Array(")
				.append(numCol)
				.append("));");
			} else {
				tmp.append("new Array(")
				.append(numCol)
				.append("),");
			}
		}

		openCode.append(tmp).append("\r")
		//.append("var data = eval(\"(\" + inputParams[\"DETAILS\"] + \")\").data;").append("\r")
		.append("var data = inputParams[\"DETAILS\"].data;").append("\r")
		.append("for (var i =0;i < data.length;i++){").append("\r");
			for (int j = 0; j < meta.length(); j++) {
				openCode.append("sourcedata").append(name).append("[i][")
				.append(j)
				.append("] = data[i][\"")
				.append(meta.getJSONObject(j).getString("name"))
				.append("\"];").append("\r");
			}
		openCode.append("}");
		return openCode;

	}
	
	
	private void createStyles() throws SemanticException, JSONException {
		if (!template.has("styles")) {
			return;
		}
		StyleHandle newStyle;
		JSONArray styles = template.getJSONArray("styles");
		for (int i = 0; i < styles.length(); i++) {
			JSONObject style = styles.getJSONObject(i);
			newStyle = designHandle.getElementFactory().newStyle(
					style.getString("name"));
			for (int x = 0; x < style.names().length(); x++) {
				String property = style.names().getString(x);
				if (!property.equals("name") && !property.equalsIgnoreCase("format")) {
					newStyle.setProperty(property, style.get(property));
				} else if (property.equalsIgnoreCase("format")){
					if (style.getString(property).equalsIgnoreCase("currency")){
						newStyle.setNumberFormatCategory("currency");
						newStyle.setNumberFormat("#,##0.00{RoundingMode=HALF_UP}");
					}
				}
			}
			designHandle.getStyles().add(newStyle);
		}
		/*
		 * StyleHandle newStyle =
		 * designHandle.getElementFactory().newStyle("LabelHeader");
		 * newStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP,
		 * DesignChoiceConstants.FONT_WEIGHT_BOLD);
		 * newStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "Arial Black");
		 * newStyle.setProperty(StyleHandle.FONT_SIZE_PROP, "13px");
		 * newStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#008000");
		 * newStyle.setProperty(StyleHandle.COLOR_PROP, "#FFFFFF");
		 * designHandle.getStyles().add(newStyle);
		 * 
		 * newStyle = designHandle.getElementFactory().newStyle("LabelRow1");
		 * //newStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP,
		 * DesignChoiceConstants.FONT_WEIGHT_BOLD);
		 * newStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "Century");
		 * //newStyle.setProperty(StyleHandle.COLOR_PROP, "#009B9B");
		 * newStyle.setProperty(StyleHandle.FONT_SIZE_PROP, "12px");
		 * newStyle.setProperty(StyleHandle.BORDER_BOTTOM_STYLE_PROP, "solid");
		 * newStyle.setProperty(StyleHandle.BORDER_BOTTOM_WIDTH_PROP,"1px");
		 * newStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#F7F7F7");
		 * designHandle.getStyles().add(newStyle);
		 * 
		 * newStyle = designHandle.getElementFactory().newStyle("LabelRow2");
		 * //newStyle.setProperty(StyleHandle.FONT_WEIGHT_PROP,
		 * DesignChoiceConstants.FONT_WEIGHT_BOLD);
		 * newStyle.setProperty(StyleHandle.FONT_FAMILY_PROP, "Century");
		 * newStyle.setProperty(StyleHandle.FONT_SIZE_PROP, "12px");
		 * newStyle.setProperty(StyleHandle.BORDER_BOTTOM_STYLE_PROP, "solid");
		 * newStyle.setProperty(StyleHandle.BORDER_BOTTOM_WIDTH_PROP,"1px");
		 * newStyle.setProperty(StyleHandle.BACKGROUND_COLOR_PROP, "#E6E5E5");
		 * designHandle.getStyles().add(newStyle);
		 */
	}
	
	private void createBody(JSONArray meta,JSONArray data) throws JSONException, ContentException, NameException, SemanticException, IOException {	
		JSONArray body = this.template.getJSONArray("body");
		for (int i = 0; i < body.length();i++){
			JSONObject element = body.getJSONObject(i);
			designHandle.getBody().add(new ReportItem(designHandle, element, meta).getReportItem());
			
		}		
	}

}