package mstn.clearg.reports;
/*
import java.io.File;
import java.util.List;

import org.eclipse.birt.core.framework.Platform;
import org.eclipse.birt.report.model.api.CellHandle;
import org.eclipse.birt.report.model.api.DesignElementHandle;
import org.eclipse.birt.report.model.api.ElementFactory;
import org.eclipse.birt.report.model.api.ExtendedItemHandle;
import org.eclipse.birt.report.model.api.GridHandle;
import org.eclipse.birt.report.model.api.IDesignEngine;
import org.eclipse.birt.report.model.api.PropertyHandle;
import org.eclipse.birt.report.model.api.ReportDesignHandle;
import org.eclipse.birt.report.model.api.RowHandle;
import org.eclipse.birt.report.model.api.ScriptDataSetHandle;
import org.eclipse.birt.report.model.api.ScriptDataSourceHandle;
import org.eclipse.birt.report.model.api.SessionHandle;
import org.eclipse.birt.report.model.api.StructureFactory;
import org.eclipse.birt.report.model.api.StyleHandle;
import org.eclipse.birt.report.model.api.TextItemHandle;
import org.eclipse.birt.report.model.api.activity.SemanticException;
import org.eclipse.birt.report.model.api.elements.DesignChoiceConstants;
import org.eclipse.birt.report.model.api.elements.structures.ResultSetColumn;
import org.eclipse.birt.report.model.api.extension.ExtendedElementException;
*/
//import org.fao.fenix.domain.label.CommodityLabel;
//import org.fao.fenix.domain.label.GaulLabel;
//import org.fao.fenix.web.birt.util.BirtUtil;
//import org.fao.fenix.web.birt.util.MatchValue;
//import org.fao.fenix.web.client.chartwizard.WizardBean;
//import org.fao.fenix.web.server.ResourceExplorerServiceImpl;

public class GraphEngine {
	/*
	ResourceExplorerServiceImpl resourceExplorerServiceImpl;
	
	ReportDesignHandle designHandle = null;

	IDesignEngine designEngine = null;

	ElementFactory designFactory = null;

	private WizardBean dataGWT;

	public GraphEngine(WizardBean dataGWT,ResourceExplorerServiceImpl resourceExplorerServiceImpl) {
		this.dataGWT = dataGWT;
		this.resourceExplorerServiceImpl=resourceExplorerServiceImpl;
	}

	public String createReport() {

		SessionHandle session = BirtUtil.getDesignSessionHandle();
		String nameReport = null;

		try {

			designHandle = session.createDesign();

			designFactory = designHandle.getElementFactory();

			DesignElementHandle simpleMasterPage = designFactory
					.newSimpleMasterPage("Master Page");//$NON-NLS-1$
			designHandle.getMasterPages().add(simpleMasterPage);

			createDataSources();
			createDataSets();

			createBody();

			
			//RptdesignToString report = new RptdesignToString(designHandle);
			//rep = report.getRptdesign();
			
			
			nameReport=BirtUtil.randomNameFile();
			designHandle.saveAs(System.getProperty("java.io.tmpdir") + File.separator + nameReport);
			
			designHandle.close();
			Platform.shutdown();

			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nameReport;
	}

	private void createDataSources() throws SemanticException {

		ScriptDataSourceHandle dataSourceHandle = designHandle
				.getElementFactory().newScriptDataSource("srcScripted");
		designHandle.getDataSources().add(dataSourceHandle);
	}

	private void createDataSets() throws SemanticException {

		// Data Set
		ScriptDataSetHandle dataSetHandle = designHandle.getElementFactory()
				.newScriptDataSet("setScripted");
		dataSetHandle.setDataSource("srcScripted");

		// Set open( ) in code
		StringBuffer openCode = generateOpenCode();
		dataSetHandle.setOpen(openCode.toString());

		// Set fetch( ) in code
		StringBuffer fetchCode = generateFetchCode();
		dataSetHandle.setFetch(fetchCode.toString());

		PropertyHandle computedSet = dataSetHandle
				.getPropertyHandle(ScriptDataSetHandle.RESULT_SET_PROP);

		// Since this is a Scripted Data Source you need to tell the
		// DataSet what the columns are. For this example, we will just
		// hard code the known resultColumn values

		ResultSetColumn resultColumn = StructureFactory.createResultSetColumn();
		resultColumn.setPosition(0);
		resultColumn.setColumnName("label");
		resultColumn.setDataType("string");
		computedSet.addItem(resultColumn);
		
		

		if (dataGWT.getChartType().equals("Pie")){
			resultColumn = StructureFactory.createResultSetColumn();
			resultColumn.setPosition(1);
			resultColumn.setColumnName("labelY");
			resultColumn.setDataType("string");
			computedSet.addItem(resultColumn);
			
			resultColumn = StructureFactory.createResultSetColumn();
			resultColumn.setPosition(2);
			resultColumn.setColumnName("value");
			resultColumn.setDataType("integer");
			computedSet.addItem(resultColumn);
		}else{
			for (int i = 0; i < dataGWT.getDimensionValuesY().size(); i++) {
				resultColumn = StructureFactory.createResultSetColumn();
				resultColumn.setPosition((i+1));
				resultColumn.setColumnName("value"+i);
				resultColumn.setDataType("integer");
				computedSet.addItem(resultColumn);
			}
			
			for (int i = 0; i < dataGWT.getDimensionValuesYForLine().size(); i++) {
				resultColumn = StructureFactory.createResultSetColumn();
				resultColumn.setPosition((dataGWT.getDimensionValuesY().size()+i+1));
				resultColumn.setColumnName("value"+ (dataGWT.getDimensionValuesY().size()+i));
				resultColumn.setDataType("integer");
				computedSet.addItem(resultColumn);
			}
			
			
		}
		

		designHandle.getDataSets().add(dataSetHandle);

	}

	private StringBuffer generateFetchCode() {
		StringBuffer fetchCode = new StringBuffer();
		fetchCode.append("if ( count < sourcedata.length ){").append("\n\t");
		fetchCode.append("row[\"label\"] = sourcedata[count][0];").append(
				"\n\t");
		
		if (dataGWT.getChartType().equals("Pie")){
			fetchCode.append("row[\"labelY\"] = sourcedata[count][1];").append(
			"\n\t");
			fetchCode.append("row[\"value\"] = sourcedata[count][2];").append(
			"\n\t");
		}else{
			for (int i=0; i < dataGWT.getDimensionValuesY().size(); i++){
				fetchCode.append("row[\"value"+ i +"\"] = sourcedata[count]["+ (i+1) +"];").append(
				"\n\t");
			}
			for (int i=0; i< dataGWT.getDimensionValuesYForLine().size(); i++){
				fetchCode.append("row[\"value"+ (dataGWT.getDimensionValuesY().size()+i) +"\"] = sourcedata[count]["+ (dataGWT.getDimensionValuesY().size()+i+1) +"];").append(
				"\n\t");
			}
		}
		
		fetchCode.append("count++;").append("\n\t");
		fetchCode.append("return true;").append("\n");
		fetchCode.append("} else {").append("\n\t");
		fetchCode.append("return false;").append("\n");
		fetchCode.append("};");
		return fetchCode;
	}

	private StringBuffer generateOpenCode() {

		StringBuffer openCode = new StringBuffer();

		openCode.append("count = 0;").append("\n");

		openCode.append(MatchValue.getValue(dataGWT,resourceExplorerServiceImpl));

		return openCode;

	}

	private ExtendedItemHandle callGraph(String typeChart) throws ExtendedElementException{
		ExtendedItemHandle handle=null;
		if (typeChart.equals("Bar")){
			handle=new BarGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Line")){
			handle=new LineGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Scatter")){
			handle=new ScatterGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Pie")){
			handle=new PieGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Tube")){
			handle=new TubeGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Cone")){
			handle=new ConeGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Pyramid")){
			handle=new PyramidGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("BarLine")){
			handle=new BarLineGraph().createChart(designHandle,dataGWT);
		} else if (typeChart.equals("Area")){
			handle=new AreaGraph().createChart(designHandle,dataGWT);
		}
		
		return handle;
	}
	
	private void createBody() {
		ElementFactory elementFactory = designHandle.getElementFactory();

		// Grid 1
		try {
			
			GridHandle dataGridHandle;
			
			if (dataGWT.getOtherDimension().size() > 0){
				dataGridHandle = elementFactory.newGridItem("dataGrid",
						1, 2);
			} else{
				dataGridHandle = elementFactory.newGridItem("dataGrid",
						1, 1);
			}
			
			dataGridHandle.setWidth("100%");
			designHandle.getBody().add(dataGridHandle);

			// Add the pie chart to the right grid pane
			RowHandle row1 = (RowHandle) dataGridHandle.getRows().get(0);
			CellHandle gridCellHandle = (CellHandle) row1.getCells().get(0);
			gridCellHandle.setProperty(StyleHandle.TEXT_ALIGN_PROP,
					DesignChoiceConstants.TEXT_ALIGN_CENTER);
			
			gridCellHandle.getContent().add(callGraph(dataGWT.getChartType()));
			
			
			//designHandle.getBody().add(callGraph(dataGWT.getChartType()));
			
			if (dataGWT.getOtherDimension().size() > 0){
				TextItemHandle parameter=designHandle.getElementFactory().newTextItem("Par");
				parameter.setContentType(DesignChoiceConstants.TEXT_DATA_CONTENT_TYPE_HTML);
				String textOtherDim="<span style='font-weight:bold; color:#FF0000;'>Parameters: &nbsp;</span>";
				for (int i=0; i<dataGWT.getOtherDimension().size(); i++){
					textOtherDim+="<span style='font-style:italic;'>"+((List)dataGWT.getOtherDimension().get(i)).get(0)+"</span>";
					if (((List)dataGWT.getOtherDimension().get(i)).get(0).equals("commodityCode")){
						textOtherDim+=": "+CommodityLabel.getLable((String)((List)dataGWT.getOtherDimension().get(i)).get(1))+"&nbsp;&nbsp;";
					} else if (((List)dataGWT.getOtherDimension().get(i)).get(0).equals("featureCode")){
						textOtherDim+=": "+GaulLabel.getLable((String)((List)dataGWT.getOtherDimension().get(i)).get(1))+"&nbsp;&nbsp;";
					}
					
				}
				parameter.setContent(textOtherDim);
			
				//designHandle.getBody().add(parameter);
				RowHandle row2 = (RowHandle) dataGridHandle.getRows().get(1);
				CellHandle gridCellHandle2 = (CellHandle) row2.getCells().get(0);
				gridCellHandle.setProperty(StyleHandle.TEXT_ALIGN_PROP,
						DesignChoiceConstants.TEXT_ALIGN_CENTER);
				
				gridCellHandle2.getContent().add(parameter);
			
			}
			
			
			
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
*/
}